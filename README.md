# VX Linux
<img src="https://gitlab.com/dessington/vx-linux/-/raw/main/vx-desktop.png">

# Features
- Appmaker for your favourite online services
- Automatic optimisation tailored to your system
- Cleanup your system easily
- Fontsizer to set global font for desktop, editor and terminal
- Kernel locked to latest stable kernel (currently 6.7)
- Load or save custom desktop layouts
- Modular system to keep your installation light
- MyIP to get your local and global IP address easily
- Optimal disk scheduling set automatically
- Pipewire as default sound server
- Remove unndeeded firmware easily
- Sync your computer to your current location easily
- Uniform theming and minimal bling bling
- Update Notifier - choice of custom (default) or OctoXBPS

# Modules
Antivirus, Bluetooth, Flatpak, Printing, Steam,  Ventoy, VirtManager, Virtualbox, Wine

# 3rd Party Installers
Installers for popular software not found in the repos located at /usr/share/vx/installers. Right click on the script and select 'Run as Root' to install in a terminal.

# Download
https://mega.nz/folder/SxwFTCTZ#s_qwDWrSWr7ZSTBVJC9Q_g

# Virtualbox
VBoxSVGA display mode is required for Layout Manager to function correctly.

# Support
support@vxlinux.org
