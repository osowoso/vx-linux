# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PROMPT_COMMAND='printf "\033]0;%s%s\007" "${PWD/#$HOME/"~"}"'
PS1='# '

alias ls='ls --color=auto'
alias install='xbps-install -Su'
alias logout='qdbus org.kde.ksmserver /KSMServer logout 0 0 1'
alias query='xbps-query -Rs'
alias remove='xbps-remove -Ro'
alias update='xbps-install -uy xbps && sudo xbps-install -Su'

export NO_AT_BRIDGE=1
