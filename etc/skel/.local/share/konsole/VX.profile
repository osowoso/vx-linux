[Appearance]
ColorScheme=nord
Font=Hack,10,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=2

[General]
Name=VX
Parent=FALLBACK/
ShowTerminalSizeHint=false
TerminalMargin=8

[Scrolling]
ScrollBarPosition=2
