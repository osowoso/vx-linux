# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PROMPT_COMMAND='printf "\033]0;%s%s\007" "${PWD/#$HOME/"~"}"'
PS1='$ '

alias ls='ls --color=auto'
alias install='sudo xbps-install -Su'
alias logout='qdbus org.kde.ksmserver /KSMServer logout 0 0 1'
alias query='sudo xbps-query -Rs'
alias remove='sudo xbps-remove -ROo'
alias update='sudo xbps-install -Su'

export NO_AT_BRIDGE=1
